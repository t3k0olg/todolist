import React from 'react'
import './index.css'
import data from './data.json'
import Products from './components/Products';

//feature 1
class  App extends React.Component {
  constructor(){
    super();
    this.state = {
      products: data.products,
      size: "",
      sort: ""
    }
  }
  render(){
    return (
      <div className="grid-container">
        <header>
          <a href="/"> Ract Shoping cart</a>
        </header>
        <main>
          <div className="content">
            <div className="main">
              <Products products={this.state.products}/>
            </div>
            <div className="sidebar">
              Cart Items
            </div>
          </div>
        </main>
        <footer>
            All rights reserve
        </footer>
      </div>
    )
  }
  
}

export default App;
